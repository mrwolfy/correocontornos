
package boletin13.pkg1;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Adri y Diego
 */
public class BoletinCorreo {
    //Adrián: Creación del ArrayList
    static ArrayList<Correo> buzon1 = new ArrayList<>();
    static int orden;
    public static void pedir_Orden(){ 
        String cadena = JOptionPane.showInputDialog("OPCIONES: \n\t 0 - Finalizar programa \n\t 1 - Redactar nuevo correo \n 2 - Ver correo \n 3 - Ver no leído\n 4 - Ver numero de mensajes \n 5 - Eliminar mensaje \n 6 - Ver numero de mensajes no leidos \n 7 - Eliminar todos los mensajes leidos \n");
        if(cadena==null){           
            System.out.println("Fin del programa");
            System.exit(0);
        }else{
            orden = Integer.parseInt(cadena);
        }  
    }
    //Adrián: Método para añadir el correo al array
    public static void añadir(Correo c){
           c.contenido = JOptionPane.showInputDialog("REDACTA TU CORREO:");
           buzon1.add(c);      
    }
    //Diego: Metodo que cuenta los mensajes no leidos
    public static void no_Leidos(){
        int conta_NoLeidos =0;
        for(int conta=0;conta<buzon1.size();conta++){
            if(buzon1.get(conta).leido==false){
                conta_NoLeidos++;
            }
        }
        if(conta_NoLeidos==0){
            JOptionPane.showMessageDialog(null, "No tienes ningún mensaje sin leer");
        }else{
            JOptionPane.showMessageDialog(null, "Tienes "+conta_NoLeidos+" mensajes sin leer.");
        }
    }
    //Adrián: Método para visualizar el Correo no Leído
    public static void ver_PrimerNoLeido(){
        boolean existe_NoLeido = false;
        for(int conta=0;conta<buzon1.size();conta++){
            if(buzon1.get(conta).leido==false){       
                JOptionPane.showMessageDialog(null, "Contenido del mensaje nº "+(conta+1)+":\n ''"+buzon1.get(conta).contenido+"''");
                buzon1.get(conta).leido = true;
                existe_NoLeido = true;
                break;
            }
        }
        if(existe_NoLeido==false){
            JOptionPane.showMessageDialog(null, "No hay ningún mensaje sin leer.");
        } 
    }
    //Diego: Metodo para leer un mensaje determinado
    public static void ver(){
       String cadena =JOptionPane.showInputDialog("Tienes "+buzon1.size()+" correos en el buzón. \n ¿Cuál deseas leer? (selecciona el numero)");
       int elemento;
       if(cadena!=null){
            elemento = Integer.parseInt(cadena);
            if(elemento>buzon1.size() || elemento<1){
                JOptionPane.showMessageDialog(null, "No hay ningun correo en la posición indicada.");
            }else{
                JOptionPane.showMessageDialog(null, "Contenido del mensaje nº "+elemento+":\n ''"+buzon1.get(elemento-1).contenido+"''");
                buzon1.get(elemento-1).leido = true;
            }    
       }
    }
    //Diego: Metodo para eliminar los mensajes leidos
    public static void eliminar_Leidos(){
        int contador =0;
         for(int conta=0;conta<buzon1.size();conta++){
            if(buzon1.get(conta).leido==true){   
                buzon1.remove(conta);
                contador++;
                conta--;
            }
         }
         if(contador!=0){
            JOptionPane.showMessageDialog(null, "Se han eliminado "+contador+" mensajes leídos.");
         }else{
            JOptionPane.showMessageDialog(null, "No se ha eliminado ningún mensaje.");
         }
    }
    //Adrián: Método para eliminar el correo que se desee
    public static void eliminar(){
            String cadena =JOptionPane.showInputDialog("Introduce el mensaje que deseas eliminar. (numero de posición):");
            
            if(cadena!=null){           
       
            int elemento = Integer.parseInt(cadena);
            
            if(elemento>buzon1.size() || elemento<1){
                JOptionPane.showMessageDialog(null, "No hay ningun correo en la posición indicada.");
            }else{
                buzon1.remove(elemento-1);
            }
       }  
    }
   
    public static void main(String[] args) {
        //Adrián: Menú de opciones con los métodos relacionados.
        pedir_Orden();
        while(orden!=0){ 
            switch(orden){   
                case 1: 
                        Correo correo1 = new Correo();
                        añadir(correo1);
                    break;
                case 2:
                        ver();
                    break;
                case 3:
                        ver_PrimerNoLeido();
                    break;
                case 4:
                    JOptionPane.showMessageDialog(null, "Tienes "+buzon1.size()+" mensajes.");
                    break;
                case 5:
                    eliminar();
                    break;
                case 6:
                    no_Leidos();
                    break;
                case 7:
                    eliminar_Leidos();
                    break;
                default:
                   JOptionPane.showMessageDialog(null, "Selecciona una opción válida.");     
            }
             pedir_Orden();
        }
    }
    
}
